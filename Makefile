NASM=nasm
LD=ld
RM=rm
FLAGS=-felf64

%.o: %.asm
	$(NASM) -g $(FLAGS) -o $@ $<

all: main
main: main.o lib.o dict.o
	$(LD) -o programm $+

.phony: all clean
clean:
	$(RM) -fr *.o
