%include "lib.inc"
%define NEXT_WORD 8

global find_word

section .text

find_word:
	xor rax, rax ;Чистим аккумулятор
    .loop:
        test rsi, rsi ;Проверка на конец словаря
        je .end
        push rsi ; сохранение адреса текущего элемента
        push rdi ; сохранение самого элемента
        add rsi, NEXT_WORD
        call string_equals
        pop rdi
        pop rsi
        cmp rax, 1
        je .break
        mov rsi, [rsi] ; адрес следующего элемента
        jmp .loop
    .break:
        mov rax, rsi
        ret
    .end:
        ret ; Очищаем аккумулятор в начале других функций, тут это делать не будем
