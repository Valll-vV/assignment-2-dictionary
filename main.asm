%define MAX_SIZE 256
%define NEXT_WORD 8
%define NEW_STRING 10;

section .bss
buff: resb MAX_SIZE

section .rodata
%include "colon.inc"
%include "words.inc"
%include "lib.inc"
start:
db "Слово для поиска (с пробельным символом в конце для корректной работы)", NEW_STRING, 0
not_found:
db "Значение не найдено", NEW_STRING, 0
error:
db "Слово слишком длинное", NEW_STRING, 0
extern find_word

section .text

global _start

_start:
    mov rdi, start
    call print_string
    mov rsi, MAX_SIZE
    mov rdi, buff
    call read_word
    test rax, rax
    je .error

    mov rsi, next_elem
    mov rdi, rax
    call find_word
    test rax, rax
    je .undefined

    .all_good:
        mov rdi, rax
        add rdi, NEXT_WORD
        push rdi
        call string_length
        pop rdi
        add rdi, rax
        inc rdi
        call print_string
        call print_newline
        call exit

    .error:
        mov rdi, error
        jmp .print_err

    .undefined:
        mov rdi, not_found

    .print_err:
        mov rsi,rdi
        push rdi
        call string_length
        pop rdi
        mov rdx, rax
        mov rdi,2
        mov rax,1
        syscall
        call exit
